﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using BingMapsRESTToolkit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PhysiciansMicroService.Models;
using RestSharp;
using Address = PhysiciansMicroService.Models.Address;

namespace PhysiciansMicroService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PhysiciansController : ControllerBase
    {
        private static string _clientId;
        private static string _clientSecret;
        private static ILogger<PhysiciansController> _logger = null;
        private static string _bingUrl;
        private static string _providersUrl;
        private static string _doctorImageUrl;
        private static string _audience;
        private static string SpecialtyUrl;
        private static string _languageUrl;

        public PhysiciansController(IConfiguration c, ILogger<PhysiciansController> logger)
        {
            _clientId = c.GetSection("ClientID").Value;
            _clientSecret = c.GetSection("ClientSecret").Value;
            _bingUrl = c.GetSection("BingUrl").Value;
            _providersUrl = c.GetSection("ProvidersUrl").Value;
            _doctorImageUrl = c.GetSection("DoctorImageUrl").Value;
            _audience = c.GetSection("Audience").Value;

            SpecialtyUrl = c.GetSection("SpecialtyUrl").Value;
            _languageUrl = c.GetSection("LanguagesUrl").Value;
            _logger = logger;

        }

        // GET: api/Physicians
        [HttpGet]
        public string Get()
        {
            const string message = "Physicians Micro Service Started....";
            _logger.LogInformation(message);
            return message;
        }

        [HttpGet]
        [Route("GetHospitals")]
        public ActionResult GetHospitals()
        {
            try
            {
                _logger.LogInformation("Starting Getting Hospitals from Service");
                // Get Auth Token
                var response = GetAuthToken();
                if (response != null)
                {
                    var token = JsonConvert.DeserializeObject<AccessToken>(response);

                    // Search through Providers for hospitals
                    var providers = GetProviders(token);
                    if (providers != null)
                    {
                        var hospitalList = new List<_HospitalMapping>();
                        _logger.LogInformation("Getting Hospitals");
                        var hospitals = providers.Where(x => x.HospitalMapping != null).Select(x => x.HospitalMapping).ToList();

                        foreach (var i in hospitals.SelectMany(item => item.Where(i => hospitalList.All(x => x.HospitalCode != i.HospitalCode))))
                        {
                            hospitalList.Add(i);
                        }

                        return Ok(hospitalList);
                    }

                    _logger.LogError("Bad Request getting Hospitals....");
                    return BadRequest();
                }

                _logger.LogError("Bad Request getting Auth Token.....");
                return BadRequest();
            }
            catch (Exception e)
            {
                _logger.LogError("Exception Occurred in GetHospitals() " + e.Message + " " + e.StackTrace, e);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetGenders")]
        public ActionResult GetGenders()
        {
            try
            {
                _logger.LogInformation("Starting Getting Genders from Service");
                // Get Auth Token
                var response = GetAuthToken();
                if (response != null)
                {
                    var token = JsonConvert.DeserializeObject<AccessToken>(response);

                    // Search through Providers for genders
                    var providers = GetProviders(token);
                    if (providers != null)
                    {
                        _logger.LogInformation("Getting Genders");
                        var genders = providers.Where(x => x.GenderId != null).Select(x => x.GenderId).Distinct().ToList();

                        return Ok(genders);
                    }

                    _logger.LogError("Bad Request getting Genders....");
                    return BadRequest();
                }

                _logger.LogError("Bad Request getting Auth Token.....");
                return BadRequest();
            }
            catch (Exception e)
            {
                _logger.LogError("Exception Occurred in Genders() " + e.Message + " " + e.StackTrace, e);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetProviders")]
        public ActionResult GetProviders()
        {
            try
            {
                _logger.LogInformation("Starting Getting Providers from Service");
                // Get Auth Token
                var response = GetAuthToken();
                if (response != null)
                {
                    var token = JsonConvert.DeserializeObject<AccessToken>(response);

                    // Get Providers
                    var providers = GetProviders(token);
                    if (providers != null)
                    {
                        _logger.LogInformation("Finished Getting Providers from Service");
                        return Ok(providers);
                    }

                    _logger.LogError("Bad Request getting Providers....");
                    return BadRequest();
                }

                _logger.LogError("Bad Request getting Auth Token.....");
                return BadRequest();
            }
            catch (Exception e)
            {
                _logger.LogError("Exception Occurred in GetProviders() " + e.Message + " " + e.StackTrace, e);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetLanguages")]
        public ActionResult GetLanguages()
        {
            _logger.LogInformation("Started Getting Languages ... ");
            try
            {
                var resp = GetAuthToken();
                if (resp != null)
                {
                    _logger.LogInformation("Start Getting Auth Token..............");
                    var token = JsonConvert.DeserializeObject<AccessToken>(resp);

                    _logger.LogInformation("Getting Languages ... ");
                    var client = new RestClient(_languageUrl);
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("authorization", "Bearer " + token.access_token);

                    var response = client.Execute(request);

                    if (response.IsSuccessful)
                    {
                        var content = response.Content;
                        _logger.LogInformation("Languages received successfully .........");
                        var languages = JsonConvert.DeserializeObject<List<Language>>(content);

                        //Add English because it's not included in the API
                        var language = new Language
                        {
                            Code = "EN",
                            Text = "English"
                        };

                        languages.Add(language);
                        return Ok(languages);
                    }

                    _logger.LogError("Error retrieving Languages " + response.StatusCode + " " + response.StatusDescription, response);
                    return BadRequest();
                }

                _logger.LogError("Bad Request getting Auth Token.....");
                return BadRequest();
            }
            catch (Exception e)
            {
                _logger.LogError("Exception Occurred in GetLanguages() " + e.Message + " " + e.StackTrace, e);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetSpecialties")]
        public ActionResult GetSpecialties()
        {
            try
            {
                _logger.LogInformation("Start Getting Auth Token..............");
                var resp = GetAuthToken();
                if (resp != null)
                {
                    var token = JsonConvert.DeserializeObject<AccessToken>(resp);
                    _logger.LogInformation("Start Getting Specialties..............");
                    var client = new RestClient(SpecialtyUrl);
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("authorization", "Bearer " + token.access_token);

                    var response = client.Execute(request);

                    if (response.IsSuccessful)
                    {
                        var content = response.Content;
                        _logger.LogInformation("Specialties received successfully .........");
                        var specialties = JsonConvert.DeserializeObject<List<Specialty>>(content);
                        return Ok(specialties);
                    }

                    _logger.LogError("Error retrieving Specialties " + response.StatusCode + " " + response.StatusDescription, response);
                    return BadRequest();
                }

                _logger.LogError("Bad Request getting Auth Token.....");
                return BadRequest();
            }
            catch (Exception e)
            {
                _logger.LogError("Exception Occurred in GetSpecialties() " + e.Message + " " + e.StackTrace, e);
                return BadRequest();
            }
        }

        public string GetAuthToken()
        {
            try
            {
                _logger.LogInformation("Start Getting Auth Token..............");
                var client = new RestClient("https://baptistjax.auth0.com/oauth/token");
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");

                //Build Parameter String
                var parameter = new AccessParameter
                {
                    client_id = _clientId,
                    client_secret = _clientSecret,
                    audience = _audience,
                    grant_type = "client_credentials"
                };

                request.AddParameter("application/json", JsonConvert.SerializeObject(parameter), ParameterType.RequestBody);
                var response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var content = response.Content;
                    _logger.LogInformation("Auth Token received successfully.......");
                    return content;
                }

                _logger.LogError("Error Getting Auth Token. " + response.StatusCode + " " + response.StatusDescription, response);
                return null;
            }
            catch (Exception e)
            {
                _logger.LogError("Exception occurred getting Auth Token " + e.Message + " " + e.InnerException, e);
                return null;
            }
        }

        public List<Provider> GetProviders(AccessToken token)
        {
            _logger.LogInformation("Getting Providers....");
            try
            {
                var client = new RestClient(_providersUrl);

                var request = new RestRequest(Method.GET);
                request.AddHeader("authorization", "Bearer " + token.access_token);

                var response = client.Execute(request);

                if (response.IsSuccessful)
                {
                    var content = response.Content;
                    _logger.LogInformation("Providers received successfully .........");
                    return GetItems(content);
                }

                _logger.LogError("Error retrieving providers " + response.StatusCode + " " + response.StatusDescription, response);
                return null;
            }
            catch (Exception e)
            {
                _logger.LogError("Exception occurred while getting Providers " + e.Message + " " + e.InnerException, e);
                return null;
            }
        }

        public List<Provider> GetItems(string content)
        {
            var providersList = new List<Provider>();
            var result = JArray.Parse(content);

            foreach (var r in result.Children())
            {
                try
                {
                    var provider = new Provider();

                    //Populate main Provider Model
                    try
                    {
                        provider.Id = (int)r.Children<JProperty>().FirstOrDefault(x => x.Name == "id").Value;
                        provider.Guid = r.Children<JProperty>().FirstOrDefault(x => x.Name == "guid").Value == null ? "" : r.Children<JProperty>().FirstOrDefault(x => x.Name == "guid").Value.ToString();
                        provider.FirstName = r.Children<JProperty>().FirstOrDefault(x => x.Name == "firstName").Value == null ? "" : r.Children<JProperty>().FirstOrDefault(x => x.Name == "firstName").Value.ToString();
                        provider.MiddleName = r.Children<JProperty>().FirstOrDefault(x => x.Name == "middleName").Value.ToString();
                        provider.LastName = r.Children<JProperty>().FirstOrDefault(x => x.Name == "lastName").Value.ToString();
                        provider.FormattedCommonName = r.Children<JProperty>().FirstOrDefault(x => x.Name == "formattedCommonName").Value.ToString();
                        provider.FormattedLastNameFirst = r.Children<JProperty>().FirstOrDefault(x => x.Name == "formattedLastNameFirst").Value.ToString();
                        provider.EchoPhysicianId = r.Children<JProperty>().FirstOrDefault(x => x.Name == "echoPhysicianId").Value.ToString();

                        //if (!string.IsNullOrEmpty(r.Children<JProperty>().FirstOrDefault(x => x.Name == "echoDoctorNumber").Value.ToString()))
                        //{
                        //    provider.EchoDoctorNumber = r.Children<JProperty>()
                        //        .FirstOrDefault(x => x.Name == "echoDoctorNumber").Value.ToString();
                        //}
                        //else
                        //{
                        //    if (r.Children<JProperty>().FirstOrDefault(x => x.Name == "symplrProviderId").Value.ToString() != null || r.Children<JProperty>().FirstOrDefault(x => x.Name == "symplrProviderId").Value.ToString() != "")
                        //    {
                        //        provider.EchoDoctorNumber = r.Children<JProperty>()
                        //            .FirstOrDefault(x => x.Name == "symplrProviderId").Value.ToString()
                        //            .Replace("`", "");
                        //    }
                        //}
                        
                        provider.EchoDoctorNumber = r.Children<JProperty>().FirstOrDefault(x => x.Name == "echoDoctorNumber").Value.ToString();
                        provider.NationalId = r.Children<JProperty>().FirstOrDefault(x => x.Name == "nationalId").Value.ToString();
                        provider.ContactPhone = r.Children<JProperty>().FirstOrDefault(x => x.Name == "contactPhone").Value == null ? "" : r.Children<JProperty>().FirstOrDefault(x => x.Name == "contactPhone").Value.ToString();
                        provider.ContactCellPhone = r.Children<JProperty>().FirstOrDefault(x => x.Name == "contactCellPhone").Value == null ? "" : r.Children<JProperty>().FirstOrDefault(x => x.Name == "contactCellPhone").Value.ToString();
                        provider.ContactPager = r.Children<JProperty>().FirstOrDefault(x => x.Name == "contactPager").Value == null ? "" : r.Children<JProperty>().FirstOrDefault(x => x.Name == "contactPager").Value.ToString();
                        provider.GenderId = r.Children<JProperty>().FirstOrDefault(x => x.Name == "genderId").Value.ToString();
                        //provider.DoctorImage = r.Children<JProperty>().FirstOrDefault(x => x.Name == "doctorImage").Value.ToString();
                        //provider.DoctorHeaderImage = r.Children<JProperty>().FirstOrDefault(x => x.Name == "doctorHeaderImage").Value.ToString();
                        provider.ThumbnailPhoto = r.Children<JProperty>().FirstOrDefault(x => x.Name == "thumbnailPhoto").Value.ToString();
                        provider.EchoSuffix = r.Children<JProperty>().FirstOrDefault(x => x.Name == "echoSuffix").Value.ToString();
                        provider.UrlRoute = r.Children<JProperty>().FirstOrDefault(x => x.Name == "urlRoute").Value.ToString();
                        provider.FormattedUrlRoute = r.Children<JProperty>().FirstOrDefault(x => x.Name == "formattedUrlRoute").Value.ToString();
                        provider.IsAcceptingNewPatients = (bool)r.Children<JProperty>().FirstOrDefault(x => x.Name == "isAcceptingNewPatients").Value;
                        provider.IsAcceptingNewPatientsVisible = (bool)r.Children<JProperty>().FirstOrDefault(x => x.Name == "isAcceptingNewPatientsVisible").Value;
                        provider.TypeId = (int)r.Children<JProperty>().FirstOrDefault(x => x.Name == "typeId").Value;
                        provider.Introduction = r.Children<JProperty>().FirstOrDefault(x => x.Name == "introduction").Value.ToString();
                        provider.FormattedSpecialties = r.Children<JProperty>().FirstOrDefault(x => x.Name == "formattedSpecialties").Value.ToString();
                        provider.IsArchived = (bool)r.Children<JProperty>().FirstOrDefault(x => x.Name == "isArchived").Value;
                        provider.IsHide = (bool)r.Children<JProperty>().FirstOrDefault(x => x.Name == "isHide").Value;
                        provider.NameOverride = r.Children<JProperty>().FirstOrDefault(x => x.Name == "nameOverride").Value.ToString();
                        provider.JobTitleOverride = r.Children<JProperty>().FirstOrDefault(x => x.Name == "jobTitleOverride").Value.ToString();
                        provider.FormattedJobTitle = r.Children<JProperty>().FirstOrDefault(x => x.Name == "formattedJobTitle").Value.ToString();
                        provider.LegalPracticeName = r.Children<JProperty>().FirstOrDefault(x => x.Name == "legalPracticeName").Value.ToString();
                        provider.DoesAcceptsEapps = (bool)r.Children<JProperty>().FirstOrDefault(x => x.Name == "doesAcceptsEapps").Value;
                        provider.IsPCMH = (bool)r.Children<JProperty>().FirstOrDefault(x => x.Name == "isPCMH").Value;
                        provider.IsBloodless = (bool)r.Children<JProperty>().FirstOrDefault(x => x.Name == "isBloodless").Value;
                        provider.AdminApproved = (bool)r.Children<JProperty>().FirstOrDefault(x => x.Name == "adminApproved").Value;
                        provider.IsNewDoctor = (bool)r.Children<JProperty>().FirstOrDefault(x => x.Name == "isNewDoctor").Value;
                        provider.IsBPP = (bool)r.Children<JProperty>().FirstOrDefault(x => x.Name == "isBPP").Value;
                        provider.IsEmployed = false; // default
                        provider.DoesHavePatientReviews = (bool)r.Children<JProperty>().FirstOrDefault(x => x.Name == "doesHavePatientReviews").Value;
                        provider.PublicationQuery = r.Children<JProperty>().FirstOrDefault(x => x.Name == "publicationQuery").Value.ToString();
                        provider.LastUpdateDateTime = r.Children<JProperty>().FirstOrDefault(x => x.Name == "lastUpdateDateTime").Value.ToString();
                        provider.LastUpdateUser = r.Children<JProperty>().FirstOrDefault(x => x.Name == "lastUpdateUser").Value.ToString();
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Exception occurred mapping Provider Info for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                            provider.FirstName + " " + provider.FormattedCommonName + " " +
                            e.Message + e.StackTrace + " Continue to next provider ", e);
                        continue;
                    }

                    //Populate Type 
                    try
                    {
                        provider.Type = new _Type
                        {
                            Code = r.Children<JProperty>().FirstOrDefault(x => x.Name == "type").Value
                                .Children<JProperty>().FirstOrDefault(x => x.Name == "code").Value.ToString(),

                            Id = (int)r.Children<JProperty>().FirstOrDefault(x => x.Name == "type").Value
                                .Children<JProperty>().FirstOrDefault(x => x.Name == "id").Value,

                            Text = r.Children<JProperty>().FirstOrDefault(x => x.Name == "type").Value
                                .Children<JProperty>().FirstOrDefault(x => x.Name == "text").Value.ToString()
                        };
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Exception occurred mapping Type for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                            provider.FirstName + " " + provider.FormattedCommonName + " " +
                            e.Message + e.StackTrace + " Continue to next provider ", e);
                        continue;
                    }

                    //Populate Groups Mapping
                    try
                    {
                        provider.GroupsMapping = new List<_GroupsMapping>();
                        foreach (var group in r.Children<JProperty>().FirstOrDefault(x => x.Name == "groupsMapping").Value)
                        {
                            try
                            {
                                var mapping = new _GroupsMapping
                                {
                                    Code = @group.Children<JProperty>().FirstOrDefault(x => x.Name == "code").Value.ToString(),
                                    Text = @group.Children<JProperty>().FirstOrDefault(x => x.Name == "text").Value.ToString(),
                                    Type = @group.Children<JProperty>().FirstOrDefault(x => x.Name == "type").Value.ToString(),
                                    OrderId = (int)@group.Children<JProperty>().FirstOrDefault(x => x.Name == "orderId").Value,
                                    IsPrimary = (bool)@group.Children<JProperty>().FirstOrDefault(x => x.Name == "isPrimary").Value
                                };
                                provider.GroupsMapping.Add(mapping);
                                if (mapping.Type.Equals("PRACTICE"))
                                {
                                    provider.IsEmployed = true;
                                }
                            }
                            catch (Exception e)
                            {
                                _logger.LogError("Exception occurred mapping GroupsMapping (loop) for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                           provider.FirstName + " " + provider.FormattedCommonName + " " +
                                           e.Message + e.StackTrace + " Continue to next GroupsMapping record for this provider ", e);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Exception occurred mapping GroupsMapping for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                            provider.FirstName + " " + provider.FormattedCommonName + " " +
                            e.Message + e.StackTrace + " Continue to next provider ", e);
                        continue;
                    }

                    //Populate Specialty Mapping
                    try
                    {
                        provider.SpecialtyMapping = new List<_SpecialtyMapping>();
                        foreach (var specialty in r.Children<JProperty>().FirstOrDefault(x => x.Name == "specialtyMapping").Value)
                        {
                            try
                            {
                                var mapping = new _SpecialtyMapping
                                {
                                    SpecialtyCode = specialty.Children<JProperty>().FirstOrDefault(x => x.Name == "specialtyCode").Value.ToString(),
                                    Text = specialty.Children<JProperty>().FirstOrDefault(x => x.Name == "text").Value.ToString(),
                                    JobTitle = specialty.Children<JProperty>().FirstOrDefault(x => x.Name == "jobTitle").Value.ToString(),
                                    SpecialtyStatus = specialty.Children<JProperty>().FirstOrDefault(x => x.Name == "specialtyStatus").Value.ToString(),
                                    SpecialtyType = specialty.Children<JProperty>().FirstOrDefault(x => x.Name == "specialtyType").Value.ToString(),
                                    HippaTaxonomy = specialty.Children<JProperty>().FirstOrDefault(x => x.Name == "hippaTaxonomy").Value.ToString(),
                                    IsBoardCertified = (bool)specialty.Children<JProperty>().FirstOrDefault(x => x.Name == "isBoardCertified").Value,
                                    IsPrimary = (bool)specialty.Children<JProperty>().FirstOrDefault(x => x.Name == "isPrimary").Value
                                };
                                provider.SpecialtyMapping.Add(mapping);
                            }
                            catch (Exception e)
                            {
                                _logger.LogError("Exception occurred mapping SpecialtyMapping (loop) for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                           provider.FirstName + " " + provider.FormattedCommonName + " " +
                                           e.Message + e.StackTrace + " Continue to next SpecialtyMapping record for this provider ", e);
                                continue;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Exception occurred mapping SpecialtyMapping for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                            provider.FirstName + " " + provider.FormattedCommonName + " " +
                            e.Message + e.StackTrace + " Continue to next provider ", e);
                        continue;
                    }

                    //Populate Language Mapping
                    try
                    {
                        provider.LanguageMapping = new List<_LanguageMapping>();

                        //Add English if not in the mapping(Add English First)
                        if (!provider.LanguageMapping.Exists(x => x.Text == "English"))
                        {
                            var mapping = new _LanguageMapping
                            {
                                LanguageCode = "EN",
                                Text = "English"
                            };
                            provider.LanguageMapping.Add(mapping);

                        }
                        foreach (var language in r.Children<JProperty>().FirstOrDefault(x => x.Name == "languageMapping").Value)
                        {
                            try
                            {
                                var mapping = new _LanguageMapping
                                {
                                    LanguageCode = language.Children<JProperty>().FirstOrDefault(x => x.Name == "languageCode").Value.ToString(),
                                    Text = language.Children<JProperty>().FirstOrDefault(x => x.Name == "text").Value.ToString()
                                };
                                provider.LanguageMapping.Add(mapping);
                            }
                            catch (Exception e)
                            {
                                _logger.LogError("Exception occurred mapping LanguageMapping (loop) for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                           provider.FirstName + " " + provider.FormattedCommonName + " " +
                                           e.Message + e.StackTrace + " Continue to next LanguageMapping record for this provider ", e);
                                continue;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Exception occurred mapping LanguageMapping for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                            provider.FirstName + " " + provider.FormattedCommonName + " " +
                            e.Message + e.StackTrace + " Continue to next provider ", e);
                        continue;
                    }

                    //Populate Education Mapping
                    try
                    {
                        provider.EducationMapping = new List<_EducationMapping>();
                        foreach (var education in r.Children<JProperty>().FirstOrDefault(x => x.Name == "educationMapping").Value)
                        {
                            try
                            {
                                var mapping = new _EducationMapping
                                {
                                    DegreeCode = education.Children<JProperty>().FirstOrDefault(x => x.Name == "degreeCode").Value.ToString(),
                                    DegreeText = education.Children<JProperty>().FirstOrDefault(x => x.Name == "degreeText").Value.ToString(),
                                    ProgramCode = education.Children<JProperty>().FirstOrDefault(x => x.Name == "programCode").Value.ToString(),
                                    ProgramText = education.Children<JProperty>().FirstOrDefault(x => x.Name == "programText").Value.ToString(),
                                    InsitutionCode = education.Children<JProperty>().FirstOrDefault(x => x.Name == "institutionCode").Value.ToString(),
                                    InstitutionText = education.Children<JProperty>().FirstOrDefault(x => x.Name == "institutionText").Value.ToString(),
                                    InstitutionCity = education.Children<JProperty>().FirstOrDefault(x => x.Name == "institutionCity").Value.ToString(),
                                    InstitutionState = education.Children<JProperty>().FirstOrDefault(x => x.Name == "institutionState").Value.ToString(),
                                    Graduate_Complete = education.Children<JProperty>().FirstOrDefault(x => x.Name == "graduate_Complete").Value.ToString(),
                                    StartYear = education.Children<JProperty>().FirstOrDefault(x => x.Name == "startYear").Value.ToString(),
                                    EndYear = education.Children<JProperty>().FirstOrDefault(x => x.Name == "endYear").Value.ToString(),
                                    SequenceId = education.Children<JProperty>().FirstOrDefault(x => x.Name == "sequenceId").Value.ToString()
                                };
                                provider.EducationMapping.Add(mapping);

                            }
                            catch (Exception e)
                            {
                                _logger.LogError("Exception occurred mapping EducationMapping (loop) for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                           provider.FirstName + " " + provider.FormattedCommonName + " " +
                                           e.Message + e.StackTrace + " Continue to next EducationMapping record for this provider ", e);
                                continue;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Exception occurred mapping EducationMapping for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                            provider.FirstName + " " + provider.FormattedCommonName + " " +
                            e.Message + e.StackTrace + " Continue to next provider ", e);
                        continue;
                    }

                    //Populate Suffix Mapping
                    try
                    {
                        provider.SuffixMapping = new List<_SuffixMapping>();
                        foreach (var suffix in r.Children<JProperty>().FirstOrDefault(x => x.Name == "suffixMapping").Value)
                        {
                            try
                            {
                                var mapping = new _SuffixMapping
                                {
                                    SuffixCode = suffix.Children<JProperty>().FirstOrDefault(x => x.Name == "suffixCode").Value.ToString(),
                                    OrderId = (int)suffix.Children<JProperty>().FirstOrDefault(x => x.Name == "orderId").Value
                                };
                                provider.SuffixMapping.Add(mapping);
                            }
                            catch (Exception e)
                            {
                                _logger.LogError("Exception occurred mapping SuffixMapping (loop) for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                           provider.FirstName + " " + provider.FormattedCommonName + " " +
                                           e.Message + e.StackTrace + " Continue to next SuffixMapping record for this provider ", e);
                                continue;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Exception occurred mapping SuffixMapping for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                            provider.FirstName + " " + provider.FormattedCommonName + " " +
                            e.Message + e.StackTrace + " Continue to next provider ", e);
                        continue;
                    }

                    //Populate Hospital Mapping
                    try
                    {
                        provider.HospitalMapping = new List<_HospitalMapping>();
                        foreach (var hospital in r.Children<JProperty>().FirstOrDefault(x => x.Name == "hospitalMapping").Value)
                        {
                            try
                            {
                                var mapping = new _HospitalMapping
                                {
                                    HospitalCode = hospital.Children<JProperty>().FirstOrDefault(x => x.Name == "hospitalCode").Value.ToString(),
                                    HospitalName = hospital.Children<JProperty>().FirstOrDefault(x => x.Name == "hospitalName").Value.ToString(),
                                    StaffCode = hospital.Children<JProperty>().FirstOrDefault(x => x.Name == "staffCode").Value.ToString(),
                                    OrderId = hospital.Children<JProperty>().FirstOrDefault(x => x.Name == "orderId").Value.ToString()
                                };
                                provider.HospitalMapping.Add(mapping);
                            }
                            catch (Exception e)
                            {
                                _logger.LogError("Exception occurred mapping HospitalMapping (loop) for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                           provider.FirstName + " " + provider.FormattedCommonName + " " +
                                           e.Message + e.StackTrace + " Continue to next HospitalMapping record for this provider ", e);
                                continue;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Exception occurred mapping HospitalMapping for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                            provider.FirstName + " " + provider.FormattedCommonName + " " +
                            e.Message + e.StackTrace + " Continue to next provider ", e);
                        continue;
                    }

                    //Populate Ages Treated Mappings
                    try
                    {
                        provider.AgesTreatedMappings = new List<_AgesTreatedMapping>();
                        foreach (var ages in r.Children<JProperty>().FirstOrDefault(x => x.Name == "agesTreatedMapping").Value)
                        {
                            try
                            {
                                var mapping = new _AgesTreatedMapping
                                {
                                    Text = ages.Children<JProperty>().FirstOrDefault(x => x.Name == "text").Value.ToString(),
                                    OrderId = (int)ages.Children<JProperty>().FirstOrDefault(x => x.Name == "orderId").Value
                                };
                                provider.AgesTreatedMappings.Add(mapping);
                            }
                            catch (Exception e)
                            {
                                _logger.LogError("Exception occurred mapping AgesTreatedMapping (loop) for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                           provider.FirstName + " " + provider.FormattedCommonName + " " +
                                           e.Message + e.StackTrace + " Continue to next AgesTreatedMapping record for this provider ", e);
                                continue;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Exception occurred mapping AgesTreatedMapping for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                            provider.FirstName + " " + provider.FormattedCommonName + " " +
                            e.Message + e.StackTrace + " Continue to next provider ", e);
                        continue;
                    }

                    //Populate Patient Forms Mappings
                    try
                    {
                        provider.PatientFormsMappings = new List<_PatientFormsMapping>();
                        foreach (var forms in r.Children<JProperty>().FirstOrDefault(x => x.Name == "patientFormsMapping").Value)
                        {
                            try
                            {
                                var mapping = new _PatientFormsMapping
                                {
                                    Name = forms.Children<JProperty>().FirstOrDefault(x => x.Name == "name").Value.ToString(),
                                    Type = forms.Children<JProperty>().FirstOrDefault(x => x.Name == "type").Value.ToString(),
                                    Link = forms.Children<JProperty>().FirstOrDefault(x => x.Name == "link").Value.ToString(),
                                    Description = forms.Children<JProperty>().FirstOrDefault(x => x.Name == "description").Value.ToString(),
                                    OrderId = (int)forms.Children<JProperty>().FirstOrDefault(x => x.Name == "orderId").Value
                                };
                                provider.PatientFormsMappings.Add(mapping);
                            }
                            catch (Exception e)
                            {
                                _logger.LogError("Exception occurred mapping PatientFormsMapping (loop) for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                           provider.FirstName + " " + provider.FormattedCommonName + " " +
                                           e.Message + e.StackTrace + " Continue to next PatientFormsMapping record for this provider ", e);
                                continue;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Exception occurred mapping PatientFormsMapping for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                            provider.FirstName + " " + provider.FormattedCommonName + " " +
                            e.Message + e.StackTrace + " Continue to next provider ", e);
                        continue;
                    }

                    //If Provider is Employed
                    if (provider.IsEmployed)
                    {
                        try
                        {
                            //provider.DoctorImage = DoctorImageUrl + r.Children<JProperty>().FirstOrDefault(x => x.Name == "doctorImage").Value.ToString();
                            //provider.DoctorHeaderImage = DoctorImageUrl + r.Children<JProperty>().FirstOrDefault(x => x.Name == "doctorHeaderImage").Value.ToString();

                            if (!string.IsNullOrEmpty(r.Children<JProperty>().FirstOrDefault(x => x.Name == "doctorImage").Value.ToString()))
                            {
                                var image = r.Children<JProperty>().FirstOrDefault(x => x.Name == "doctorImage").Value.ToString();
                                var ext = Path.GetExtension(image);
                                if (!string.IsNullOrEmpty(ext))
                                {
                                    provider.DoctorImage = _doctorImageUrl + r.Children<JProperty>().FirstOrDefault(x => x.Name == "doctorImage").Value.ToString();
                                }
                                else
                                {
                                    provider.DoctorImage = null;
                                }
                            }
                            else
                            {
                                provider.DoctorImage = r.Children<JProperty>().FirstOrDefault(x => x.Name == "echoHeaderImage").Value.ToString();
                            }

                            provider.Locations = new List<Locations>();

                            var locations = r.Children<JProperty>().FirstOrDefault(x => x.Name == "locationMapping").Value.ToString();

                            //Provider doesnt have any data in locationMapping
                            if (locations != "[]")
                            {
                                foreach (var location in r.Children<JProperty>()
                            .FirstOrDefault(x => x.Name == "locationMapping").Value.Take(3))
                                {
                                    try
                                    {
                                        var mapping = new Locations
                                        {
                                            LocationId = (int)location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "locationId").Value,
                                            LocationGuid = location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "locationGuid").Value.ToString(),
                                            Name = location.Children<JProperty>().FirstOrDefault(x => x.Name == "name")
                                                    .Value.ToString(),
                                            Address1 = location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "address1").Value.ToString(),
                                            Address2 = location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "address2").Value.ToString(),
                                            Address3 = location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "address3").Value.ToString(),
                                            City = location.Children<JProperty>().FirstOrDefault(x => x.Name == "city")
                                                    .Value.ToString(),
                                            State = location.Children<JProperty>().FirstOrDefault(x => x.Name == "state")
                                                    .Value.ToString(),
                                            Zip = location.Children<JProperty>().FirstOrDefault(x => x.Name == "zip").Value
                                                    .ToString(),
                                            Phone = location.Children<JProperty>().FirstOrDefault(x => x.Name == "phone")
                                                    .Value.ToString(),
                                            Fax = location.Children<JProperty>().FirstOrDefault(x => x.Name == "fax").Value
                                                    .ToString(),
                                            IsAcceptingEAppointments = (bool)location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "isAcceptingEAppointments").Value,
                                            IsDefaultLocation = (bool)location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "isDefaultLocation").Value,
                                            SequenceId = (int)location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "sequenceId").Value,
                                            Lat = (double)location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "lat").Value,
                                            Lng = (double)location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "lng").Value
                                        };

                                        //if (mapping.Lng < 1)
                                        if (mapping.Lng == 0)
                                        {
                                            var address = new Address
                                            {
                                                StreetAddress = mapping.Address1,
                                                City = mapping.City,
                                                State = mapping.State,
                                                Zip = mapping.Zip
                                            };

                                            var zipCode = GetLatLong(address);
                                            mapping.Lat = zipCode.lat;
                                            mapping.Lng = zipCode.lon;
                                        }
                                        try
                                        {
                                            var office = "";
                                            var numbers = location.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "phoneNumber").Value;
                                            foreach (var num in numbers)
                                            {
                                                var text = num.Children<JProperty>().FirstOrDefault(x => x.Name == "text").Value
                                                    .ToString();
                                                var number = num.Children<JProperty>().FirstOrDefault(x => x.Name == "number").Value
                                                    .ToString();
                                                if (text.Equals("Main:"))
                                                {
                                                    mapping.mainPhone = number;
                                                }
                                                else if (text.Equals("Admissions:"))

                                                {
                                                    mapping.admissionsPhone = number;
                                                }
                                                else if (text.Equals("Fax:"))
                                                {
                                                    if (string.IsNullOrEmpty(mapping.Fax))
                                                    {
                                                        mapping.Fax = number;
                                                    }
                                                }
                                                else if (text.Equals("Office:"))
                                                {
                                                    office = number;
                                                }
                                            }

                                            if (string.IsNullOrEmpty(mapping.mainPhone))
                                            {
                                                mapping.mainPhone = office;
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            _logger.LogError(
                                                "Exception occurred mapping LocationMapping.PhoneNUmber for Provider DoctorNumber: " +
                                                provider.EchoDoctorNumber + " Provider Name: " +
                                                provider.FirstName + " " + provider.FormattedCommonName + " " +
                                                e.Message + e.StackTrace + " Continue to next provider ", e);
                                            continue;
                                        }

                                        try
                                        {
                                            mapping.Hours = new List<Locations._Hours>();
                                            foreach (var hours in location.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "hours").Value)
                                            {
                                                try
                                                {
                                                    var hour = new Locations._Hours
                                                    {
                                                        DayOfWeek = hours.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "dayOfWeek").Value.ToString(),
                                                        OpenTime = hours.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "openTime").Value.ToString(),
                                                        CloseTime = hours.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "closeTime").Value.ToString()
                                                    };
                                                    mapping.Hours.Add(hour);
                                                }
                                                catch (Exception e)
                                                {
                                                    _logger.LogError(
                                                        "Exception occurred mapping Hours (loop) for Provider DoctorNumber: " +
                                                        provider.EchoDoctorNumber + " Provider Name: " +
                                                        provider.FirstName + " " + provider.FormattedCommonName + " " +

                                                        e.Message + e.StackTrace +
                                                        " Continue to next Hours Record for this provider ", e);
                                                    continue;
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            _logger.LogError(
                                                "Exception occurred mapping LocationMapping.Hours for Provider DoctorNumber: " +
                                                provider.EchoDoctorNumber + " Provider Name: " +
                                                provider.FirstName + " " + provider.FormattedCommonName + " " +
                                                e.Message + e.StackTrace + " Continue to next provider ", e);
                                            continue;
                                        }

                                        try
                                        {
                                            mapping.OtherProviders = new List<Locations._Providers>();
                                            foreach (var other in location.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "otherProviders").Value)
                                            {
                                                try
                                                {
                                                    var otherInfo = new Locations._Providers
                                                    {
                                                        Guid = other.Children<JProperty>().FirstOrDefault(x=>x.Name=="guid").Value.ToString(),
                                                        CommonName = other.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "commonName").Value.ToString(),
                                                        UrlRoute = other.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "urlRoute").Value.ToString(),
                                                        JobTitle = other.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "jobTitle").Value.ToString()
                                                    };
                                                    mapping.OtherProviders.Add(otherInfo);
                                                }
                                                catch (Exception e)
                                                {
                                                    _logger.LogError(
                                                        "Exception occurred mapping Providers (loop) for Provider DoctorNumber: " +
                                                        provider.EchoDoctorNumber + " Provider Name: " +
                                                        provider.FirstName + " " + provider.FormattedCommonName + " " +

                                                        e.Message + e.StackTrace +
                                                        " Continue to next Provider Record for this provider ", e);
                                                    continue;
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            _logger.LogError(
                                                "Exception occurred mapping LocationMapping.Providers for Provider DoctorNumber: " +
                                                provider.EchoDoctorNumber + " Provider Name: " +
                                                provider.FirstName + " " + provider.FormattedCommonName + " " +
                                                e.Message + e.StackTrace + " Continue to next provider ", e);
                                            continue;
                                        }

                                        try
                                        {
                                            mapping.OfficeExtenders = new List<Locations._Providers>();
                                            foreach (var extenders in location.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "officeExtenders").Value)
                                            {
                                                try
                                                {
                                                    var extender = new Locations._Providers
                                                    {
                                                        Guid = extenders.Children<JProperty>().FirstOrDefault(x=>x.Name=="guid").Value.ToString(),
                                                        CommonName = extenders.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "commonName").Value.ToString(),
                                                        UrlRoute = extenders.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "urlRoute").Value.ToString(),
                                                        JobTitle = extenders.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "jobTitle").Value.ToString()
                                                    };
                                                    mapping.OfficeExtenders.Add(extender);
                                                }
                                                catch (Exception e)
                                                {
                                                    _logger.LogError(
                                                        "Exception occurred mapping Extenders (loop) for Provider DoctorNumber: " +
                                                        provider.EchoDoctorNumber + " Provider Name: " +
                                                        provider.FirstName + " " + provider.FormattedCommonName + " " +

                                                        e.Message + e.StackTrace +
                                                        " Continue to next Extenders Record for this provider ", e);
                                                    continue;
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            _logger.LogError(
                                                "Exception occurred mapping LocationMapping.OfficeExtenders for Provider DoctorNumber: " +
                                                provider.EchoDoctorNumber + " Provider Name: " +
                                                provider.FirstName + " " + provider.FormattedCommonName + " " +
                                                e.Message + e.StackTrace + " Continue to next provider ", e);
                                            continue;
                                        }

                                        provider.Locations.Add(mapping);

                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e);
                                        throw;
                                    }
                                }
                            }
                            //Grabbing data for provider from addresses
                            else
                            {
                                var count = 0;

                                var addressList = r.Children<JProperty>().FirstOrDefault(x => x.Name == "addresses").Value.ToList();
                                var sortedList = addressList.OrderBy(x =>
                                        x.Children<JProperty>().FirstOrDefault(x => x.Name == "addressType").Value
                                            .ToString())
                                    .ToList();
                                foreach (var address in sortedList)
                                {
                                    try
                                    {
                                        if (address.Children<JProperty>().FirstOrDefault(x => x.Name == "addressType").Value
                                            .ToString() != "Home Address" && address.Children<JProperty>().FirstOrDefault(x => x.Name == "addressType").Value
                                            .ToString() != "Billing Address" && address.Children<JProperty>().FirstOrDefault(x => x.Name == "addressType").Value
                                            .ToString() != "Mailing Address")
                                        {
                                            var addresses = new Locations
                                            {
                                                Address1 = address.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "address1").Value.ToString(),
                                                Address2 = address.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "address2").Value.ToString(),
                                                Address3 = address.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "address3").Value.ToString(),
                                                City = address.Children<JProperty>().FirstOrDefault(x => x.Name == "city").Value
                                                .ToString(),
                                                Zip = address.Children<JProperty>().FirstOrDefault(x => x.Name == "zip").Value
                                                .ToString(),
                                                State = address.Children<JProperty>().FirstOrDefault(x => x.Name == "state")
                                                .Value.ToString(),
                                                mainPhone = address.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "phone1").Value.ToString(),
                                                admissionsPhone =
                                                address.Children<JProperty>().FirstOrDefault(x => x.Name == "phone2")
                                                    .Value == null
                                                    ? ""
                                                    : address.Children<JProperty>().FirstOrDefault(x => x.Name == "phone2")
                                                        .Value.ToString(),
                                                Fax = address.Children<JProperty>().FirstOrDefault(x => x.Name == "fax").Value
                                                .ToString(),
                                                Lat = string.IsNullOrEmpty(address.Children<JProperty>().FirstOrDefault(x => x.Name == "lat").Value.ToString()) ? 0 : (double)address.Children<JProperty>().FirstOrDefault(x => x.Name == "lat").Value,
                                                Lng = string.IsNullOrEmpty(address.Children<JProperty>().FirstOrDefault(x => x.Name == "lng").Value.ToString()) ? 0 : (double)address.Children<JProperty>().FirstOrDefault(x => x.Name == "lng").Value,
                                                type = address.Children<JProperty>().FirstOrDefault(x => x.Name == "addressType").Value
                                                .ToString(),
                                                Name = provider.LegalPracticeName
                                            };
                                            if (addresses.Lat == 0)
                                            {
                                                // get lat long
                                                var location = new Address
                                                {
                                                    StreetAddress = addresses.Address1,
                                                    City = addresses.City,
                                                    State = addresses.State,
                                                    Zip = addresses.Zip

                                                };
                                                var zipCode = GetLatLong(location);
                                                addresses.Lat = zipCode.lat;
                                                addresses.Lng = zipCode.lon;
                                            }
                                            if (count == 0)
                                            {
                                                addresses.IsDefaultLocation = true;
                                            }
                                            else
                                            {
                                                addresses.IsDefaultLocation = false;
                                            }

                                            var addressExists = provider.Locations.Any(x =>
                                                x.Address1 == addresses.Address1 && x.Address2 == addresses.Address2 &&
                                                x.Address3 == addresses.Address3 && x.City == addresses.City &&
                                                x.State == addresses.State && x.Zip == addresses.Zip);

                                            if (!addressExists)
                                            {
                                                provider.Locations.Add(addresses);
                                            }
                                            count++;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        _logger.LogError("Exception occurred mapping Addresses (loop) for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                                  provider.FirstName + " " + provider.FormattedCommonName + " " +
                                                  e.Message + e.StackTrace + " Continue to next Address Record for this provider ", e);
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.LogError("Exception occurred mapping Locations for Provider DoctorNumber: " +
                                             provider.EchoDoctorNumber + " Provider Name: " +
                                             provider.FirstName + " " + provider.FormattedCommonName + " " +
                                             e.Message + e.StackTrace + " Continue to next Location ", e);
                            continue;
                        }

                    }
                    //Provider is not employed
                    else
                    {
                        try
                        {
                            provider.DoctorImage = r.Children<JProperty>().FirstOrDefault(x => x.Name == "echoHeaderImage").Value.ToString();
                            provider.DoctorHeaderImage = r.Children<JProperty>().FirstOrDefault(x => x.Name == "echoHeaderImage").Value.ToString();

                            provider.Locations = new List<Locations>();
                            var count = 0;
                            var addressList = r.Children<JProperty>().FirstOrDefault(x => x.Name == "addresses").Value.ToList();
                            var sortedList = addressList.OrderBy(x =>
                                    x.Children<JProperty>().FirstOrDefault(x => x.Name == "addressType").Value
                                        .ToString())
                                .ToList();

                            var hasAddress = r.Children<JProperty>().FirstOrDefault(x => x.Name == "addresses").Value.ToString();

                            //Provider doesnt have any data in addresses
                            if (hasAddress != "[]")
                            {
                                foreach (var address in sortedList)
                                {
                                    try
                                    {
                                        if (address.Children<JProperty>().FirstOrDefault(x => x.Name == "addressType").Value
                                            .ToString() != "Home Address" && address.Children<JProperty>().FirstOrDefault(x => x.Name == "addressType").Value
                                            .ToString() != "Billing Address" && address.Children<JProperty>().FirstOrDefault(x => x.Name == "addressType").Value
                                            .ToString() != "Mailing Address")
                                        {
                                            var addresses = new Locations
                                            {
                                                Address1 = address.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "address1").Value.ToString(),
                                                Address2 = address.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "address2").Value.ToString(),
                                                Address3 = address.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "address3").Value.ToString(),
                                                City = address.Children<JProperty>().FirstOrDefault(x => x.Name == "city").Value
                                                .ToString(),
                                                Zip = address.Children<JProperty>().FirstOrDefault(x => x.Name == "zip").Value
                                                .ToString(),
                                                State = address.Children<JProperty>().FirstOrDefault(x => x.Name == "state")
                                                .Value.ToString(),
                                                mainPhone = address.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "phone1").Value.ToString(),
                                                admissionsPhone =
                                                address.Children<JProperty>().FirstOrDefault(x => x.Name == "phone2")
                                                    .Value == null
                                                    ? ""
                                                    : address.Children<JProperty>().FirstOrDefault(x => x.Name == "phone2")
                                                        .Value.ToString(),
                                                Fax = address.Children<JProperty>().FirstOrDefault(x => x.Name == "fax").Value
                                                .ToString(),
                                                Lat = string.IsNullOrEmpty(address.Children<JProperty>().FirstOrDefault(x => x.Name == "lat").Value.ToString()) ? 0 : (double)address.Children<JProperty>().FirstOrDefault(x => x.Name == "lat").Value,
                                                Lng = string.IsNullOrEmpty(address.Children<JProperty>().FirstOrDefault(x => x.Name == "lng").Value.ToString()) ? 0 : (double)address.Children<JProperty>().FirstOrDefault(x => x.Name == "lng").Value,
                                                type = address.Children<JProperty>().FirstOrDefault(x => x.Name == "addressType").Value
                                                .ToString(),
                                                Name = provider.LegalPracticeName
                                            };
                                            if (addresses.Lat == 0)
                                            {
                                                // get lat long
                                                var location = new Address
                                                {
                                                    StreetAddress = addresses.Address1,
                                                    City = addresses.City,
                                                    State = addresses.State,
                                                    Zip = addresses.Zip

                                                };
                                                var zipCode = GetLatLong(location);
                                                addresses.Lat = zipCode.lat;
                                                addresses.Lng = zipCode.lon;
                                            }
                                            if (count == 0)
                                            {
                                                addresses.IsDefaultLocation = true;
                                            }
                                            else
                                            {
                                                addresses.IsDefaultLocation = false;
                                            }
                                            var addressExists = provider.Locations.Any(x =>
                                                x.Address1 == addresses.Address1 && x.Address2 == addresses.Address2 &&
                                                x.Address3 == addresses.Address3 && x.City == addresses.City &&
                                                x.State == addresses.State && x.Zip == addresses.Zip);

                                            if (!addressExists)
                                            {
                                                provider.Locations.Add(addresses);
                                            }
                                            count++;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        _logger.LogError("Exception occurred mapping Addresses (loop) for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                                  provider.FirstName + " " + provider.FormattedCommonName + " " +
                                                  e.Message + e.StackTrace + " Continue to next Address Record for this provider ", e);
                                    }
                                }
                            }
                            else
                            {
                                foreach (var location in r.Children<JProperty>()
                            .FirstOrDefault(x => x.Name == "locationMapping").Value.Take(3))
                                {
                                    try
                                    {
                                        var mapping = new Locations
                                        {
                                            LocationId = (int)location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "locationId").Value,
                                            LocationGuid = location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "locationGuid").Value.ToString(),
                                            Name = location.Children<JProperty>().FirstOrDefault(x => x.Name == "name")
                                                    .Value.ToString(),
                                            Address1 = location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "address1").Value.ToString(),
                                            Address2 = location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "address2").Value.ToString(),
                                            Address3 = location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "address3").Value.ToString(),
                                            City = location.Children<JProperty>().FirstOrDefault(x => x.Name == "city")
                                                    .Value.ToString(),
                                            State = location.Children<JProperty>().FirstOrDefault(x => x.Name == "state")
                                                    .Value.ToString(),
                                            Zip = location.Children<JProperty>().FirstOrDefault(x => x.Name == "zip").Value
                                                    .ToString(),
                                            Phone = location.Children<JProperty>().FirstOrDefault(x => x.Name == "phone")
                                                    .Value.ToString(),
                                            Fax = location.Children<JProperty>().FirstOrDefault(x => x.Name == "fax").Value
                                                    .ToString(),
                                            IsAcceptingEAppointments = (bool)location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "isAcceptingEAppointments").Value,
                                            IsDefaultLocation = (bool)location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "isDefaultLocation").Value,
                                            SequenceId = (int)location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "sequenceId").Value,
                                            Lat = (double)location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "lat").Value,
                                            Lng = (double)location.Children<JProperty>()
                                                    .FirstOrDefault(x => x.Name == "lng").Value
                                        };

                                        //if (mapping.Lng < 1)
                                        if (mapping.Lng == 0)
                                        {
                                            var address = new Address
                                            {
                                                StreetAddress = mapping.Address1,
                                                City = mapping.City,
                                                State = mapping.State,
                                                Zip = mapping.Zip
                                            };

                                            var zipCode = GetLatLong(address);
                                            mapping.Lat = zipCode.lat;
                                            mapping.Lng = zipCode.lon;
                                        }
                                        try
                                        {
                                            var office = "";
                                            var numbers = location.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "phoneNumber").Value;
                                            foreach (var num in numbers)
                                            {
                                                var text = num.Children<JProperty>().FirstOrDefault(x => x.Name == "text").Value
                                                    .ToString();
                                                var number = num.Children<JProperty>().FirstOrDefault(x => x.Name == "number").Value
                                                    .ToString();
                                                if (text.Equals("Main:"))
                                                {
                                                    mapping.mainPhone = number;
                                                }
                                                else if (text.Equals("Admissions:"))

                                                {
                                                    mapping.admissionsPhone = number;
                                                }
                                                else if (text.Equals("Fax:"))
                                                {
                                                    if (string.IsNullOrEmpty(mapping.Fax))
                                                    {
                                                        mapping.Fax = number;
                                                    }
                                                }
                                                else if (text.Equals("Office:"))
                                                {
                                                    office = number;
                                                }
                                            }

                                            if (string.IsNullOrEmpty(mapping.mainPhone))
                                            {
                                                mapping.mainPhone = office;
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            _logger.LogError(
                                                "Exception occurred mapping LocationMapping.PhoneNUmber for Provider DoctorNumber: " +
                                                provider.EchoDoctorNumber + " Provider Name: " +
                                                provider.FirstName + " " + provider.FormattedCommonName + " " +
                                                e.Message + e.StackTrace + " Continue to next provider ", e);
                                            continue;
                                        }

                                        try
                                        {
                                            mapping.Hours = new List<Locations._Hours>();
                                            foreach (var hours in location.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "hours").Value)
                                            {
                                                try
                                                {
                                                    var hour = new Locations._Hours
                                                    {
                                                        DayOfWeek = hours.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "dayOfWeek").Value.ToString(),
                                                        OpenTime = hours.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "openTime").Value.ToString(),
                                                        CloseTime = hours.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "closeTime").Value.ToString()
                                                    };
                                                    mapping.Hours.Add(hour);
                                                }
                                                catch (Exception e)
                                                {
                                                    _logger.LogError(
                                                        "Exception occurred mapping Hours (loop) for Provider DoctorNumber: " +
                                                        provider.EchoDoctorNumber + " Provider Name: " +
                                                        provider.FirstName + " " + provider.FormattedCommonName + " " +

                                                        e.Message + e.StackTrace +
                                                        " Continue to next Hours Record for this provider ", e);
                                                    continue;
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            _logger.LogError(
                                                "Exception occurred mapping LocationMapping.Hours for Provider DoctorNumber: " +
                                                provider.EchoDoctorNumber + " Provider Name: " +
                                                provider.FirstName + " " + provider.FormattedCommonName + " " +
                                                e.Message + e.StackTrace + " Continue to next provider ", e);
                                            continue;
                                        }

                                        try
                                        {
                                            mapping.OtherProviders = new List<Locations._Providers>();
                                            foreach (var other in location.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "otherProviders").Value)
                                            {
                                                try
                                                {
                                                    var otherInfo = new Locations._Providers
                                                    {
                                                        Guid = other.Children<JProperty>().FirstOrDefault(x=>x.Name=="guid").Value.ToString(),
                                                        CommonName = other.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "commonName").Value.ToString(),
                                                        UrlRoute = other.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "urlRoute").Value.ToString(),
                                                        JobTitle = other.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "jobTitle").Value.ToString()
                                                    };
                                                    mapping.OtherProviders.Add(otherInfo);
                                                }
                                                catch (Exception e)
                                                {
                                                    _logger.LogError(
                                                        "Exception occurred mapping Providers (loop) for Provider DoctorNumber: " +
                                                        provider.EchoDoctorNumber + " Provider Name: " +
                                                        provider.FirstName + " " + provider.FormattedCommonName + " " +

                                                        e.Message + e.StackTrace +
                                                        " Continue to next Provider Record for this provider ", e);
                                                    continue;
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            _logger.LogError(
                                                "Exception occurred mapping LocationMapping.Providers for Provider DoctorNumber: " +
                                                provider.EchoDoctorNumber + " Provider Name: " +
                                                provider.FirstName + " " + provider.FormattedCommonName + " " +
                                                e.Message + e.StackTrace + " Continue to next provider ", e);
                                            continue;
                                        }

                                        try
                                        {
                                            mapping.OfficeExtenders = new List<Locations._Providers>();
                                            foreach (var extenders in location.Children<JProperty>()
                                                .FirstOrDefault(x => x.Name == "officeExtenders").Value)
                                            {
                                                try
                                                {
                                                    var extender = new Locations._Providers
                                                    {
                                                        Guid = extenders.Children<JProperty>().FirstOrDefault(x=>x.Name=="guid").Value.ToString(),
                                                        CommonName = extenders.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "commonName").Value.ToString(),
                                                        UrlRoute = extenders.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "urlRoute").Value.ToString(),
                                                        JobTitle = extenders.Children<JProperty>()
                                                            .FirstOrDefault(x => x.Name == "jobTitle").Value.ToString()
                                                    };
                                                    mapping.OfficeExtenders.Add(extender);
                                                }
                                                catch (Exception e)
                                                {
                                                    _logger.LogError(
                                                        "Exception occurred mapping Extenders (loop) for Provider DoctorNumber: " +
                                                        provider.EchoDoctorNumber + " Provider Name: " +
                                                        provider.FirstName + " " + provider.FormattedCommonName + " " +

                                                        e.Message + e.StackTrace +
                                                        " Continue to next Extenders Record for this provider ", e);
                                                    continue;
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            _logger.LogError(
                                                "Exception occurred mapping LocationMapping.OfficeExtenders for Provider DoctorNumber: " +
                                                provider.EchoDoctorNumber + " Provider Name: " +
                                                provider.FirstName + " " + provider.FormattedCommonName + " " +
                                                e.Message + e.StackTrace + " Continue to next provider ", e);
                                            continue;
                                        }

                                        provider.Locations.Add(mapping);

                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e);
                                        throw;
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.LogError("Exception occurred mapping Addresses for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                              provider.FirstName + " " + provider.FormattedCommonName + " " +
                                              e.Message + e.StackTrace + " Continue to next Provider Record ", e);
                            continue;
                        }
                    }
                    
                    //Populate Board Mapping
                    try
                    {
                        provider.BoardMapping = new List<_BoardMapping>();
                        foreach (var board in r.Children<JProperty>().FirstOrDefault(x => x.Name == "boardMapping").Value)
                        {
                            try
                            {
                                var mapping = new _BoardMapping
                                {
                                    BoardCode = board.Children<JProperty>().FirstOrDefault(x => x.Name == "boardCode").Value.ToString(),
                                    Text = board.Children<JProperty>().FirstOrDefault(x => x.Name == "text").Value.ToString(),
                                    BoardStatus = board.Children<JProperty>().FirstOrDefault(x => x.Name == "boardStatus").Value.ToString(),
                                    CertificationDate = board.Children<JProperty>().FirstOrDefault(x => x.Name == "certificationDate").Value.ToString(),
                                    RecertificationDate = board.Children<JProperty>().FirstOrDefault(x => x.Name == "recertificationDate").Value.ToString(),
                                    ExpirationDate = board.Children<JProperty>().FirstOrDefault(x => x.Name == "expirationDate").Value.ToString(),
                                    CertificationNumber = board.Children<JProperty>().FirstOrDefault(x => x.Name == "certificationNumber").Value.ToString(),
                                    UserDef_M1 = board.Children<JProperty>().FirstOrDefault(x => x.Name == "userDef_M1").Value.ToString(),
                                    Active = (bool)board.Children<JProperty>().FirstOrDefault(x => x.Name == "active").Value
                                };
                                provider.BoardMapping.Add(mapping);
                            }
                            catch (Exception e)
                            {
                                _logger.LogError("Exception occurred mapping BoardMapping (loop) for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                                      provider.FirstName + " " + provider.FormattedCommonName + " " +
                                                      e.Message + e.StackTrace + " Continue to next provider ", e);
                                continue;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Exception occurred mapping BoardMapping for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                              provider.FirstName + " " + provider.FormattedCommonName + " " +
                                              e.Message + e.StackTrace + " Continue to next provider ", e);
                            continue;
                    }

                    //Populate License Mapping
                    try
                    {
                        provider.LicenseMapping = new List<_LicenseMapping>();
                        foreach (var license in r.Children<JProperty>().FirstOrDefault(x => x.Name == "licenseMapping").Value)
                        {
                            try
                            {
                                var mapping = new _LicenseMapping
                                {
                                    LicenseType = license.Children<JProperty>()
                                        .FirstOrDefault(x => x.Name == "licenseType").Value.ToString(),
                                    LicenseStatus = license.Children<JProperty>()
                                        .FirstOrDefault(x => x.Name == "licenseStatus").Value.ToString(),
                                    State = license.Children<JProperty>().FirstOrDefault(x => x.Name == "state")
                                        .Value.ToString(),
                                    LicenseNumber = license.Children<JProperty>()
                                        .FirstOrDefault(x => x.Name == "licenseNumber").Value.ToString(),
                                    AwardDate = license.Children<JProperty>().FirstOrDefault(x => x.Name == "awardDate")
                                        .Value.ToString(),
                                    ExpirationDate = license.Children<JProperty>()
                                        .FirstOrDefault(x => x.Name == "expirationDate").Value.ToString(),
                                    Contact_Name = license.Children<JProperty>()
                                        .FirstOrDefault(x => x.Name == "contact_Name").Value.ToString(),
                                    Contact_Phone = license.Children<JProperty>()
                                        .FirstOrDefault(x => x.Name == "contact_Phone").Value.ToString(),
                                    COntact_Fax = license.Children<JProperty>()
                                        .FirstOrDefault(x => x.Name == "contact_Fax").Value.ToString(),
                                    Contact_Email = license.Children<JProperty>()
                                        .FirstOrDefault(x => x.Name == "contact_Email").Value.ToString(),
                                    LicensureField = license.Children<JProperty>()
                                        .FirstOrDefault(x => x.Name == "licensureField").Value.ToString(),
                                    Institution_Name = license.Children<JProperty>().FirstOrDefault(x => x.Name == "institution_Name")
                                        .Value.ToString(),
                                    Institution_Contact = license.Children<JProperty>().FirstOrDefault(x => x.Name == "institution_Contact")
                                        .Value.ToString(),
                                    Institution_Address1 = license.Children<JProperty>().FirstOrDefault(x => x.Name == "institution_Address1")
                                        .Value.ToString(),
                                    Institution_Address2 = license.Children<JProperty>().FirstOrDefault(x => x.Name == "institution_Address2")
                                        .Value.ToString(),
                                    Institution_City = license.Children<JProperty>().FirstOrDefault(x => x.Name == "institution_City")
                                        .Value.ToString(),
                                    Institution_State = license.Children<JProperty>().FirstOrDefault(x => x.Name == "institution_State")
                                        .Value.ToString(),
                                    Institution_Zip = license.Children<JProperty>().FirstOrDefault(x => x.Name == "institution_Zip")
                                        .Value.ToString(),
                                    Active = (bool) license.Children<JProperty>().FirstOrDefault(x => x.Name == "active")
                                        .Value
                                };
                                provider.LicenseMapping.Add(mapping);
                            }
                            catch (Exception e)
                            {
                                _logger.LogError("Exception occurred mapping LicenseMapping (loop) for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                                 provider.FirstName + " " + provider.FormattedCommonName + " " +
                                                 e.Message + e.StackTrace + " Continue to next provider ", e);
                                continue;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Exception occurred mapping LicenseMapping for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                         provider.FirstName + " " + provider.FormattedCommonName + " " +
                                         e.Message + e.StackTrace + " Continue to next provider ", e);
                        continue;
                    }

                    //Populate Expertise
                    try
                    {
                        provider.Expertise = new List<_Expertise>();
                        foreach (var expertise in r.Children<JProperty>().FirstOrDefault(x => x.Name == "expertise").Value)
                        {
                            try
                            {
                                var expertiseInfo = new _Expertise
                                {
                                    Text = expertise.Children<JProperty>().FirstOrDefault(x => x.Name == "text").Value.ToString(),
                                    OrderId = expertise.Children<JProperty>().FirstOrDefault(x => x.Name == "orderId").Value.ToString()
                                };
                                provider.Expertise.Add(expertiseInfo);
                            }
                            catch (Exception e)
                            {
                                _logger.LogError("Exception occurred mapping Expertise (loop) for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                          provider.FirstName + " " + provider.FormattedCommonName + " " +
                                          e.Message + e.StackTrace + " Continue to next Expertise Record for this provider ", e);
                                continue;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Exception occurred mapping Expertise for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                          provider.FirstName + " " + provider.FormattedCommonName + " " +
                                          e.Message + e.StackTrace + " Continue to next Hours Record for this provider ", e);
                        continue;
                    }

                    //Populate Provider Paragraphs
                    try
                    {
                        provider.ProviderParagraphs = new List<_ProviderParagraphs>();
                        foreach (var paragraphs in r.Children<JProperty>().FirstOrDefault(x => x.Name == "providerParagraphs").Value)
                        {
                            try
                            {
                                var paragraphsInfo = new _ProviderParagraphs
                                {
                                    Text = paragraphs.Children<JProperty>().FirstOrDefault(x => x.Name == "text").Value
                                        .ToString(),
                                    OrderId = paragraphs.Children<JProperty>().FirstOrDefault(x => x.Name == "orderId")
                                        .Value.ToString()
                                };
                                provider.ProviderParagraphs.Add(paragraphsInfo);
                            }
                            catch (Exception e)
                            {
                                _logger.LogError("Exception occurred mapping ProviderPragraphs (loop) for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                          provider.FirstName + " " + provider.FormattedCommonName + " " +
                                          e.Message + e.StackTrace + " Continue to next ProviderPragraphs Record for this provider ", e);
                                continue;
                            }
                        }
                        providersList.Add(provider);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Exception occurred mapping ProviderParagraphs for Provider DoctorNumber: " + provider.EchoDoctorNumber + " Provider Name: " +
                                          provider.FirstName + " " + provider.FormattedCommonName + " " +
                                          e.Message + e.StackTrace + " Continue to next provider ", e);
                        continue;
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError("An Exception occurred mapping provider. Continuing on to next provider...." + e.Message + " " + e.InnerException + " " + e.StackTrace, e);
                    continue;
                }
            }

            return providersList;
        }
        public static ZipCode GetLatLong(Address address)
        {
            var zip = new ZipCode();
            try
            {
                if (address != null && !string.IsNullOrEmpty(address.StreetAddress) && !string.IsNullOrEmpty(address.City))
                {
                    var client = new RestClient(_bingUrl);
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("Address", JsonConvert.SerializeObject(address));
                    var response = client.Execute(request);
                    if (!response.IsSuccessful) return null;

                    try
                    {
                        var coords = JsonConvert.DeserializeObject<Coordinate>(response.Content);
                        zip.lat = coords.Latitude;
                        zip.lon = coords.Longitude;
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("There was an error getting Lat/Long from Microservice " + e.Message);
                    }


                    
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("PhysiciansController :: GetLatLong -> Could not obtain coordinates " + ex.Message);
            }
            return zip;
        }
    }
}
