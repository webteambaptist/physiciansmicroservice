﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhysiciansMicroService.Models
{
    public class ZipCode
    {
        public double lat { get; set; }
        public double lon { get; set; }
        public ZipCode() { }
    }
}
