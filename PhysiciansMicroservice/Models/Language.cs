﻿
namespace PhysiciansMicroService.Models
{
    public class Language
    {
        public string Code { get; set; }
        public string Text { get; set; }
    }
}
